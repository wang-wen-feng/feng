package entity;

public class Doctor {
    private  Integer uid;
    private  String userphone;
    private  String password;
    private  String name;
    private  Integer age;
    private  String sex;
    private  Integer platform_department;
    private  Integer title;
    private  Integer hospital;
    private  Integer department;
    private  String good_at;
    private  String doctor_introduction;
    private  Integer doctor_license_id;
    private  Integer patients;
    private  Integer appointment;
    private  Integer fansNum;
    private  Integer auditStatus;
    private  String reg_time;
}
